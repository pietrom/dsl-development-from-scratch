package org.amicofragile.dsls.xmlmaster

class XmlMaster {
    private Node root;
    private Node current;

    def methodMissing(String name, args) {
        List arguments = ((Object[])args).toList()
        def attributes = arguments.size() > 1 ? arguments[0] : new HashMap<String, String>()

        def newNode = new Node(name, attributes)
        if(root == null) {
            root = current = newNode
        } else {
            current.addChild(newNode)
        }

        def body = arguments.size() > 1 ? arguments[1] : arguments[0]
        body.delegate = this
        current = newNode
        body()
        current = newNode.parent

        this
    }

    def toXml() {
        root.toXml()
    }
}

class Node {
    private final String name
    private Map<String, String> attributes = new HashMap<>()
    private final List<Node> children
    def Node parent

    Node(String name, Map<String, String> attributes = new HashMap()) {
        this.name = name
        this.attributes = attributes
        this.children = new LinkedList<>()
    }

    def addChild(Node child) {
        child.parent = this
        children.add(child)
    }

    private static final int INDENT_SIZE = 3

    def toXml(indent = 0) {
        def buffer = new StringBuilder(" " * indent).append("<$name")
        for (attribute in attributes) {
            buffer.append(" ${attribute.key}=${renderAttributeValue(attribute.value)}")
        }
        if(children) {
            buffer.append(">")
            for(child in children) {
                buffer.append("\n")
                buffer.append(child.toXml(indent + INDENT_SIZE))
            }
            buffer.append("\n")
            buffer.append(" " * indent).append("</$name>")
        } else {
            buffer.append(" />")
        }
        buffer.toString()
    }

    def renderAttributeValue(value) {
        "\"${value.toString()}\""
    }
}