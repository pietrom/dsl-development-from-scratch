package org.amicofragile.dsls.xmlmaster

import static org.hamcrest.CoreMatchers.*
import static org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class XmlMasterTest {
    @Test
    void canHandleSingleEmptyTag() {
        def result = new XmlMaster().root {
        }.toXml()
        assertThat(result, is(equalTo("<root />")))
    }

    @Test
    void canHandleSingleTagWithAttributes() {
        def result = new XmlMaster().root(a: 123, b: "xyz") {
        }.toXml()
        assertThat(result, is(equalTo("""<root a="123" b="xyz" />""")))
    }

    @Test
    void canHandleOneLevelNestedTags() {
        def result = new XmlMaster()
            .root() {
                abcd {}
                efgh {}
            }.toXml()
        assertThat(result, is(equalTo("""<root>
   <abcd />
   <efgh />
</root>""")))
    }

    @Test
    void canHandleOneLevelNestedTagsWithAttributes() {
        def result = new XmlMaster()
                .root(id: 123) {
                    abcd(x:1, y:2) {}
                    efgh(text: "abcd") {}
                    ijkl {}
                }.toXml()
        assertThat(result, is(equalTo("""<root id="123">
   <abcd x="1" y="2" />
   <efgh text="abcd" />
   <ijkl />
</root>""")))
    }

    @Test
    void canHandleMultiLevelNestedTagsWithAttributes() {
        def result = new XmlMaster()
                .root() {
                    abcd(id: 11) {
                        xxyyzz(text: "A message") {
                        }
                    }
                    efgh {
                        ijkl(id: 19, value: 100, text: "Blah blah blah") {
                            mnop {
                            }
                        }
                    }
                }.toXml()
        assertThat(result, is(equalTo("""<root>
   <abcd id="11">
      <xxyyzz text="A message" />
   </abcd>
   <efgh>
      <ijkl id="19" value="100" text="Blah blah blah">
         <mnop />
      </ijkl>
   </efgh>
</root>""")))
    }
}
