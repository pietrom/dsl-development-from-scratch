package org.amicofragile.dsls.xmlmaster

import static org.hamcrest.CoreMatchers.*
import static org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class LanguageTest {
    def aFunction(Map parameters, Closure aClosure) {
        return aClosure(parameters.size())
    }

    @Test
    void shouldSayHelloWorld() {
        def result = aFunction(anInt:19, a: "x", b:"y", c:11) { n -> "Total is $n" }
        assertThat(result.toString(), is(equalTo("Total is 4")))
    }
}
