package org.amicofragile.dsls.exthttp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.*;

public class ExtHttpResponse {
    private HttpURLConnection connection;

    public ExtHttpResponse(HttpURLConnection connection) {
        this.connection = connection;
    }

    public int getStatusCode() throws IOException {
        return connection.getResponseCode();
    }

    public String getResponseBody() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
        StringBuilder sb = new StringBuilder();
        String s = null;
        while ((s = reader.readLine()) != null) {
            sb.append(s);
        }
        return sb.toString();
    }

    public String getHeader(String key) {
        return connection.getHeaderField(key);
    }

    public String getCookie(String key) {
        Map<String, List<String>> headers = connection.getHeaderFields();
        Map<String, String> cookies = new HashMap<>();
        List<String> setCookies = Optional.ofNullable(headers.get("set-cookie")).orElse(new LinkedList<>());
        for (String setCookie : setCookies) {
            String[] fields = setCookie.trim().split("=");
            cookies.put(fields[0], fields[1]);
        }
        return cookies.get(key);
    }
}