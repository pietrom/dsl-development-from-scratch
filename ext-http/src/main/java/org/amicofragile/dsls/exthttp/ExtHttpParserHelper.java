package org.amicofragile.dsls.exthttp;

public interface ExtHttpParserHelper {
    void setUrl(String url);

    void setMethod(String method);

    void registerCookie(String key, String value);

    void registerHeader(String key, String value);

    ExtHttpRequestWrapper requestWrapper();
}
