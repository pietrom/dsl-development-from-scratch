package org.amicofragile.dsls.exthttp;

public interface ExtHttpRequestWrapper {
    ExtHttpResponse execute() throws Exception;
}
