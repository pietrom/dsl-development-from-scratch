package org.amicofragile.dsls.exthttp;

import org.amicofragile.dsls.commons.http.HttpUrlConnectionFacade;

import java.util.HashMap;
import java.util.Map;

public class ExtHttp implements ExtHttpParserHelper, ExtHttpRequestWrapper {
    private String method;
    private String url;
    private Map<String, String> headers = new HashMap<>();
    private Map<String, String> cookies = new HashMap<>();

    public ExtHttpResponse execute() throws Exception {
        return new ExtHttpResponse(HttpUrlConnectionFacade.openGet(url, headers, cookies));
    }

    @Override
    public void setUrl(String url) {
        this.url = unquoteStringLiteral(url);
    }

    @Override
    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public void registerCookie(String key, String value) {
        cookies.put(unquoteStringLiteral(key), unquoteStringLiteral(value));
    }

    @Override
    public void registerHeader(String key, String value) {
        headers.put(unquoteStringLiteral(key), unquoteStringLiteral(value));
    }

    @Override
    public ExtHttpRequestWrapper requestWrapper() {
        return this;
    }

    private String unquoteStringLiteral(String input) {
        return input.replace("\"", "");
    }
}
