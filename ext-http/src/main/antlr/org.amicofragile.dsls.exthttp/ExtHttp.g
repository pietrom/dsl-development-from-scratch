grammar ExtHttp;

@parser::header {
	package org.amicofragile.dsls.exthttp;

	import java.io.IOException;
    	import java.io.Reader;
}

@lexer::header {
	package org.amicofragile.dsls.exthttp;
}

@parser::members {
	private ExtHttpParserHelper helper;

	public ExtHttpParser(TokenStream input, ExtHttpParserHelper helper) {
		this(input);
		this.helper = helper;
	}

	public static ExtHttpParser parser(Reader reader) throws IOException {
        ExtHttpLexer lexer = new ExtHttpLexer(CharStreams.fromReader(reader));
        lexer.removeErrorListeners();
        ExceptionThrowingErrorListener listener = new ExceptionThrowingErrorListener();
        lexer.addErrorListener(listener);
        ExtHttpParser parser = new ExtHttpParser(new org.antlr.v4.runtime.CommonTokenStream(lexer), new ExtHttp());
        parser.removeErrorListeners();
        parser.addErrorListener(listener);
        return parser;
    }
}

request returns [ExtHttpRequestWrapper wrapper]: 'get' '(' url = StringLiteral ')' '{' requestBody '}'  {
    helper.setUrl($url.getText());
    helper.setMethod("GET");
    $wrapper = helper.requestWrapper();
} ;

requestBody: headers? cookies? ;

headers : 'headers' '{' header * '}' ;

header : 'add' k = StringLiteral v = StringLiteral {
    helper.registerHeader($k.getText(), $v.getText());
} ;

cookies : 'cookies' '{' cookie * '}' ;

cookie : 'add' k = StringLiteral v = StringLiteral {
    helper.registerCookie($k.getText(), $v.getText());
} ;

StringLiteral: ["](~["])*["] ;
WS : (' ' |'\t' | '\r' | '\n')+ { skip(); } ;
COMMENT : '//'(~'\n')* { skip(); } ;
ILLEGAL : . { System.out.println("Illegal input found: " + getText()); } ;