package org.amicofragile.dsls.exthttp;

import org.amicofragile.dsls.commons.http.PortUtils;
import org.antlr.v4.runtime.CharStreams;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockserver.integration.ClientAndServer;

import java.io.*;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public class ExtHttpGetTest {
    private static int localPort;
    private static ClientAndServer clientAndServer;

    @BeforeClass
    public static void InitMockServer() {
        localPort = PortUtils.nextFreePort(10000, 11000);
        clientAndServer = startClientAndServer(localPort);

        clientAndServer.when(
                request()
                        .withMethod("GET")
                        .withPath("/test")
                        .withHeader("Authorization", "Bearer 12345654321")
                        .withCookie("UserId", "pietrom")
        ).respond(
                response()
                        .withStatusCode(200)
                        .withBody("Test passed!")
                        .withCookie(
                                "sessionId", "1234321"
                        )
                        .withCookie("foo", "bar")
                        .withHeader(
                                "Location", "https://www.mock-server.com"
                        )
        );
    }

    @AfterClass
    public static void TeardownMockServer() {
        clientAndServer.stop();
    }

    @Test
    public void getStatusCodeWithExternalDsl() throws Exception {
        ExtHttpParser parser = ExtHttpParser.parser(readInput("/get.ehttp"));
        ExtHttpResponse response = parser.request().wrapper.execute();
        assertThat(response.getStatusCode(), is(equalTo(200)));
    }

    @Test
    public void getResponseBodyWithExternalDsl() throws Exception {
        ExtHttpParser parser = ExtHttpParser.parser(readInput("/get.ehttp"));
        ExtHttpResponse response = parser.request().wrapper.execute();
        assertThat(response.getResponseBody(), is(equalTo("Test passed!")));
    }

    @Test
    public void getResponseHeaderWithExternalDsl() throws Exception {
        ExtHttpParser parser = ExtHttpParser.parser(readInput("/get.ehttp"));
        ExtHttpResponse response = parser.request().wrapper.execute();
        assertThat(response.getHeader("Location"), is(equalTo("https://www.mock-server.com")));
    }

    @Test
    public void getCookiesWithExternalDsl() throws Exception {
        ExtHttpParser parser = ExtHttpParser.parser(readInput("/get.ehttp"));
        ExtHttpParser.RequestContext request = parser.request();
        ExtHttpResponse response = request.wrapper.execute();
        assertThat(response.getCookie("sessionId"), is(equalTo("1234321")));
        assertThat(response.getCookie("foo"), is(equalTo("bar")));
    }

    private Reader readInput(String resource) {
        String result = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(resource)))
                .lines().collect(Collectors.joining("\n"));
        return new StringReader(result.replace("${PORT}", Integer.toString(localPort)));
    }
}