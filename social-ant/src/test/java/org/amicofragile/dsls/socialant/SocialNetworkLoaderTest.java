package org.amicofragile.dsls.socialant;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;

import org.amicofragile.dsls.socialant.loader.*;
import org.amicofragile.dsls.socialant.loader.parser.*;
import org.antlr.v4.runtime.CharStreams;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class SocialNetworkLoaderTest implements SocialNetworkParserHelper {
    private List<Friendship> friendships;

    @Before
    public void clearFriendships() {
        friendships = new LinkedList<Friendship>();
    }

    @Test
    public void shouldParseAllFriendships() throws IOException {
        Reader input = new InputStreamReader(getClass().getResourceAsStream("/simple-network.sn"));
        SocialNetworkLexer lexer = new SocialNetworkLexer(CharStreams.fromReader(input));
        SocialNetworkParser parser = new SocialNetworkParser(new org.antlr.v4.runtime.CommonTokenStream(lexer), this);
        parser.script();
        System.out.println(friendships);
        Assert.assertEquals(3, friendships.size());
    }

    @Test
    public void shouldParseCorrectFriendshipData() throws IOException {
        Reader input = new InputStreamReader(getClass().getResourceAsStream("/simple-network.sn"));
        SocialNetworkLexer lexer = new SocialNetworkLexer(CharStreams.fromReader(input));
        SocialNetworkParser parser = new SocialNetworkParser(new org.antlr.v4.runtime.CommonTokenStream(lexer), this);
        parser.script();
        Friendship f = friendships.get(0);
        assertThat(f.left, is(equalTo("Pietro")));
        assertThat(f.right, is(equalTo("Cristina")));
        assertThat(f.year, is(equalTo(1998)));
    }

    private static final class Friendship {
        private final String left, right;
        private final int year;

        public Friendship(String left, String right, int year) {
            this.left = left;
            this.right = right;
            this.year = year;
        }

        public String getLeft() {
            return left;
        }

        public String getRight() {
            return right;
        }

        @Override
        public String toString() {
            return "Friendship [left=" + left + ", right=" + right + " since " + year + "]";
        }
    }

    @Override
    public void registerFriendship(String left, String right, SinceExpr since) {
        this.friendships.add(new Friendship(left, right, since.getYear()));
    }
}