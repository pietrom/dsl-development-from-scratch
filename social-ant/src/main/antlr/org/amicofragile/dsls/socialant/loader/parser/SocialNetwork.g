grammar SocialNetwork;

@parser::header {
	package org.amicofragile.dsls.socialant.loader.parser;

	import org.amicofragile.dsls.socialant.loader.*;
}

@lexer::header {
	package org.amicofragile.dsls.socialant.loader.parser;

	import org.amicofragile.dsls.socialant.loader.*;
}

@parser::members {
	private SocialNetworkParserHelper helper;

	public SocialNetworkParser(TokenStream input, SocialNetworkParserHelper helper) {
		this(input);
		this.helper = helper;
	}
}


script : friendship * EOF ;
friendship : left = Name 'is' 'friend' 'of' right = Name since = sinceExpr {helper.registerFriendship($left.getText(), $right.getText(), $since.expr);} ;
sinceExpr returns [SinceExpr expr]: 'since' y = Year { $expr = new SinceExpr(Integer.parseInt($y.getText())); };

Name : ('a'..'z' | 'A'..'Z')+ ;
Year: ('1' | '2')('0'..'9')('0'..'9')('0'..'9');
WS : (' ' |'\t' | '\r' | '\n')+ {skip();} ;
COMMENT : '#'(~'\n')* {skip();} ;
ILLEGAL : .;