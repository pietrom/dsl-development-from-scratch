package org.amicofragile.dsls.socialant.loader;

public class SinceExpr {
    private final int year;

    public SinceExpr(int year) {
        this.year = year;
    }

    public int getYear() {
        return year;
    }
}
