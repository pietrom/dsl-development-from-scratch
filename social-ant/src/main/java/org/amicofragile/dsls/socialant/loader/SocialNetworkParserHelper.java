package org.amicofragile.dsls.socialant.loader;

public interface SocialNetworkParserHelper {
    void registerFriendship(String left, String right, SinceExpr since);
}
