package org.amicofragile.dsls.khttp

import org.amicofragile.dsls.commons.http.PortUtils
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.equalTo
import org.junit.*
import org.junit.Assert.assertThat
import org.mockserver.integration.ClientAndServer.startClientAndServer
import org.mockserver.integration.ClientAndServer
import org.mockserver.model.HttpRequest.request
import org.mockserver.model.HttpResponse.response

class KHttpGetTest {
    companion object {
        private var localPort: Int = 0
        private var clientAndServer: ClientAndServer? = null

        @BeforeClass @JvmStatic
        fun initMockServer() {
            localPort = PortUtils.nextFreePort(10000, 11000)
            clientAndServer = startClientAndServer(localPort)
        }

        @AfterClass @JvmStatic
        fun teardownMockServer() {
            clientAndServer?.stop()
        }

        fun initMockServer(path: String, responseBody: String) {
            clientAndServer!!.reset()

            clientAndServer!!.`when`(
                    request()
                            .withMethod("GET")
                            .withPath(path)
                            .withHeader("Authorization", "Bearer 12345654321")
                            .withCookie("UserId", "pietrom")
            ).respond(
                    response()
                            .withStatusCode(200)
                            .withBody(responseBody)
                            .withCookie(
                                    "sessionId", "1234321"
                            )
                            .withCookie("foo", "bar")
                            .withHeader(
                                    "Location", "https://www.mock-server.com"
                            )
            )
        }
    }

    @Test
    fun getStatusCodeWitKotlinDsl() {
        initMockServer("/test", "Test passed!")
        khttp(String.format("http://localhost:%d/test", localPort)) {
            val response = get {
                headers {
                    set header "Authorization" to "Bearer 12345654321"
                }
                cookies {
                    set cookie "UserId" to "pietrom"
                }
            }
            assertThat(response.getStatusCode(), `is`(equalTo(200)))
        }
    }

    @Test
    fun getResponseBodyWithKotlinDsl() {
        initMockServer("/test", "Test passed!")
        khttp(String.format("http://localhost:%d/test", localPort)) {
            val response = get {
                headers {
                    set header "Authorization" to "Bearer 12345654321"
                }
                cookies {
                    set cookie "UserId" to "pietrom"
                }
            }
            assertThat<String>(response.getResponseBody(), `is`<String>(equalTo<String>("Test passed!")))
        }
    }

    @Test
    fun getResponseBodyComposingUrlWithKotlinDsl() {
        initMockServer("/test", "Test passed!")
        khttp(String.format("http://localhost:%d", localPort)) {
            val response = get("/test") {
                headers {
                    set header "Authorization" to "Bearer 12345654321"
                }
                cookies {
                    set cookie "UserId" to "pietrom"
                }
            }
            assertThat<String>(response.getResponseBody(), `is`<String>(equalTo<String>("Test passed!")))
        }
    }

    @Test
    fun getResponseBodyFromMultipleUrlsComposingUrlWithKotlinDsl() {
        initMockServer("/test", "Test passed!")
        khttp(String.format("http://localhost:%d", localPort)) {
            headers {
                set header "Authorization" to "Bearer 12345654321"
            }
            val response1 = get("/test") {
                cookies {
                    set cookie "UserId" to "pietrom"
                }
            }
            assertThat<String>(response1.getResponseBody(), `is`<String>(equalTo<String>("Test passed!")))

            initMockServer("/test2", "Test 2 passed!")

            val response2 = get("/test2") {
                cookies {
                    set cookie "UserId" to "pietrom"
                }
            }
            assertThat<String>(response2.getResponseBody(), `is`<String>(equalTo<String>("Test 2 passed!")))
        }
    }

    @Test
    fun getResponseHeaderWithKotlinDsl() {
        initMockServer("/test", "Test passed!")
        khttp(String.format("http://localhost:%d/test", localPort)) {
            val response = get {
                headers {
                    set header "Authorization" to "Bearer 12345654321"
                }
                cookies {
                    set cookie "UserId" to "pietrom"
                }
            }
            assertThat<String>(response.getHeader("Location"), `is`<String>(equalTo<String>("https://www.mock-server.com")))
        }
    }

    @Test
    fun getCookiesWithKotlinDsl() {
        initMockServer("/test", "Test passed!")
        khttp(String.format("http://localhost:%d/test", localPort)) {
            val response = get {
                headers {
                    set header "Authorization" to "Bearer 12345654321"
                }
                cookies {
                    set cookie "UserId" to "pietrom"
                }
            }
            assertThat<String>(response.getCookie("sessionId"), `is`<String>(equalTo<String>("1234321")))
            assertThat<String>(response.getCookie("foo"), `is`<String>(equalTo<String>("bar")))
        }
    }
}