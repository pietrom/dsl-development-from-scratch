package org.amicofragile.dsls.khttp

import com.sun.net.httpserver.Headers
import org.amicofragile.dsls.commons.http.HttpUrlConnectionFacade
import java.net.HttpURLConnection
import java.nio.charset.Charset
import java.util.*

fun khttp(baseUrl : String, init : KHttpContext.() -> Unit) {
    val ctx = KHttpContext(baseUrl)
    ctx.init()
}

class KHttpContext(val baseUrl: String) {
    private val headersCtx = HeadersContext(null)

    fun createContext(url: String, method: String) : KHttpRequestContext = KHttpRequestContext(url, method, headersCtx)

    fun get(init : KHttpRequestContext.() -> Unit) : KHttpResponse {
        val ctx = createContext(baseUrl,"GET")
        ctx.init()
        return ctx.doConnection()
    }

    fun get(url: String, init : KHttpRequestContext.() -> Unit) : KHttpResponse {
        val ctx = createContext("$baseUrl/$url", "GET")
        ctx.init()
        return ctx.doConnection()
    }

    fun headers(init: HeadersContext.() -> Unit) {
        headersCtx.init()
    }
}

class KHttpRequestContext(val url: String, val method: String, parentHeadersCtx: HeadersContext) {
    private val headersCtx = HeadersContext(parentHeadersCtx)
    private val cookiesCtx = CookiesContext()

    fun doConnection() : KHttpResponse {
        val conn = HttpUrlConnectionFacade.openConnection(url, method, headersCtx.items, cookiesCtx.items)
        return KHttpResponse(conn)
    }

    fun headers(init: HeadersContext.() -> Unit) {
        headersCtx.init()
    }

    fun cookies(init: CookiesContext.() -> Unit) {
        cookiesCtx.init()
    }
}

class KHttpResponse(val connection: HttpURLConnection) {
    fun getStatusCode(): Int {
        return connection.responseCode
    }

    fun getResponseBody(): String {
        return connection.inputStream.bufferedReader(Charset.forName("utf-8")).use { it.readText() }
    }

    fun getHeader(key: String): String {
        return connection.getHeaderField(key)
    }

    fun getCookie(key: String): String? {
        val headers = connection.headerFields
        val cookies = HashMap<String, String>()
        val setCookies = Optional.ofNullable(headers["set-cookie"]).orElse(LinkedList())
        for (setCookie in setCookies) {
            val fields = setCookie.trim { it <= ' ' }.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            cookies[fields[0]] = fields[1]
        }
        return cookies[key]
    }
}

fun <T> T?.orElse(other: T) : T {
    return this ?: other
}

open class KeyValueContext(initialCtx: KeyValueContext?) {
    val items : MutableMap<String, String> = mutableMapOf(* initialCtx?.items.orElse(mapOf<String, String>()).entries.map { e -> Pair(e.key, e.value) } .toTypedArray())
    protected var currentKey: String? = null

    infix fun to(value: String) {
        items[currentKey !!] = value
    }
}

class HeadersContext(initialCtx: HeadersContext?= null) : KeyValueContext(initialCtx) {
    val set = this

    infix fun header(key : String) : HeadersContext {
        currentKey = key
        return this
    }
}

class CookiesContext(initialCtx: CookiesContext? = null) : KeyValueContext(initialCtx) {
    val set = this

    infix fun cookie(key: String) : CookiesContext {
        currentKey = key
        return this
    }
}