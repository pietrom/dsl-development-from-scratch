package org.amicofragile.dsls.commons.http;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.stream.Collectors;

public class HttpUrlConnectionFacade {
    public static HttpURLConnection openGet(String url, Map<String, String> headers, Map<String, String> cookies) throws Exception {
        return openConnection(url, "GET", headers, cookies);
    }

    public static HttpURLConnection openConnection(String url, String method, Map<String, String> headers, Map<String, String> cookies) throws Exception {
        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
        conn.setRequestMethod(method);
        for (Map.Entry<String, String> header: headers.entrySet()) {
            conn.setRequestProperty(header.getKey(), header.getValue());
        }
        String cookiesHeader = cookies.entrySet().stream().map(cookie -> String.format("%s=%s", cookie.getKey(), cookie.getValue())).collect(Collectors.joining("; "));
        conn.setRequestProperty("Cookie", cookiesHeader);
        conn.connect();
        return conn;
    }
}
