package org.amicofragile.dsls.ffmaster.jdsl;

import org.amicofragile.dsls.ffmaster.core.*;

import java.util.ArrayList;
import java.util.List;

interface FieldCommandBuilder {
    FieldCommand buildCommand(String fieldId);
}

class EnableBuilder implements FieldCommandBuilder {
    @Override
    public FieldCommand buildCommand(String fieldId) {
        return new EnableCommand(fieldId);
    }
}

class DisableBuilder implements FieldCommandBuilder {
    @Override
    public FieldCommand buildCommand(String fieldId) {
        return new DisableCommand(fieldId);
    }
}

class ShowBuilder implements FieldCommandBuilder {
    @Override
    public FieldCommand buildCommand(String fieldId) {
        return new ShowCommand(fieldId);
    }
}

class HideBuilder implements FieldCommandBuilder {
    @Override
    public FieldCommand buildCommand(String fieldId) {
        return new HideCommand(fieldId);
    }
}

public class FormFieldsCommandListBuilder {
    private final List<WidgetCommand> commands;
    private FieldCommandBuilder currentBuilder;
    private List<FieldCommandBuilder> pendingBuilders;

    public FormFieldsCommandListBuilder() {
        this.commands = new ArrayList<>();
        this.pendingBuilders = new ArrayList<>();
    }

    public List<WidgetCommand> commands() {
        return commands;
    }

    public FormFieldsCommandListBuilder enable(String fieldId) {
        currentBuilder = new EnableBuilder();
        addCommand(fieldId);
        return this;
    }

    public FormFieldsCommandListBuilder disable(String fieldId) {
        currentBuilder = new DisableBuilder();
        addCommand(fieldId);
        return this;
    }

    public FormFieldsCommandListBuilder show(String fieldId) {
        currentBuilder = new ShowBuilder();
        addCommand(fieldId);
        return this;
    }

    public FormFieldsCommandListBuilder hide(String fieldId) {
        currentBuilder = new HideBuilder();
        addCommand(fieldId);
        return this;
    }

    public FormFieldsCommandListBuilder and(String fieldId) {
        addCommand(fieldId);
        return this;
    }

    private void addCommand(String fieldId) {
        commands.add(currentBuilder.buildCommand(fieldId));
    }

}
