package org.amicofragile.dsls.ffmaster.jdsl;

import org.amicofragile.dsls.ffmaster.core.*;
import org.junit.Test;

import java.util.List;

import static org.amicofragile.dsls.ffmaster.jdsl.FormFieldsMasterDsl.dsl;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class FormFieldsMasterDslTest {
    @Test
    public void canCreateEmptyCommandList() {
        List<WidgetCommand> commands = dsl().commands();
        assertThat(commands, is(emptyCollectionOf(WidgetCommand.class)));
    }

    @Test
    public void canCreateSingleEnableCommand() {
        List<WidgetCommand> commands = dsl()
                .enable("myField").commands();
        assertThat(commands, contains(new EnableCommand("myField")));
    }

    @Test
    public void canCreateSingleDisableCommand() {
        List<WidgetCommand> commands = dsl()
                .disable("myField").commands();
        assertThat(commands, contains(new DisableCommand("myField")));
    }

    @Test
    public void canCreateSingleShowCommand() {
        List<WidgetCommand> commands = dsl()
                .show("myField").commands();
        assertThat(commands, contains(new ShowCommand("myField")));
    }

    @Test
    public void canCreateSingleHideCommand() {
        List<WidgetCommand> commands = dsl()
                .hide("myField").commands();
        assertThat(commands, contains(new HideCommand("myField")));
    }

    @Test
    public void canCreateMultipleEnableCommand() {
        List<WidgetCommand> commands = dsl()
                .enable("myField0")
                .and("myField1")
                .and("myField2")
                .and("myField3")
                .commands();
        assertThat(commands, contains(new EnableCommand("myField0"), new EnableCommand("myField1"),
        new EnableCommand("myField2"), new EnableCommand("myField3")));
    }

    @Test
    public void canCreateMultipleDisableCommand() {
        List<WidgetCommand> commands = dsl()
                .disable("myField0")
                .and("myField1")
                .and("myField2")
                .and("myField3")
                .commands();
        assertThat(commands, contains(new DisableCommand("myField0"), new DisableCommand("myField1"),
                new DisableCommand("myField2"), new DisableCommand("myField3")));
    }

    @Test
    public void canCreateMultipleShowCommand() {
        List<WidgetCommand> commands = dsl()
                .show("myField0")
                .and("myField1")
                .and("myField2")
                .and("myField3")
                .commands();
        assertThat(commands, contains(new ShowCommand("myField0"), new ShowCommand("myField1"),
                new ShowCommand("myField2"), new ShowCommand("myField3")));
    }

    @Test
    public void canCreateMultipleHideCommand() {
        List<WidgetCommand> commands = dsl()
                .hide("myField0")
                .and("myField1")
                .and("myField2")
                .and("myField3")
                .commands();
        assertThat(commands, contains(new HideCommand("myField0"), new HideCommand("myField1"),
                new HideCommand("myField2"), new HideCommand("myField3")));
    }

    @Test
    public void canMixCommandOfDifferentTypes() {
        List<WidgetCommand> commands = dsl()
                .hide("myField0")
                    .and("myField1")
                .show("myField2")
                    .and("myField3")
                .enable("myField4")
                .disable("myField5")
                .show("myField6")
                .commands();
        assertThat(commands, contains(new HideCommand("myField0"), new HideCommand("myField1"),
                new ShowCommand("myField2"), new ShowCommand("myField3"),
                new EnableCommand("myField4"), new DisableCommand("myField5"),
                new ShowCommand("myField6")));
    }
}
