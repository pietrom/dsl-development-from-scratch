package org.amicofragile.dsls.ffmaster.core;

public interface WidgetCommand {
    String toCommand();
}
