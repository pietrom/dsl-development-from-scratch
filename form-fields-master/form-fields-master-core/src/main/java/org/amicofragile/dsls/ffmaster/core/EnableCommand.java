package org.amicofragile.dsls.ffmaster.core;

public class EnableCommand extends FieldCommand {
    public EnableCommand(String fieldId) {
        super(fieldId);
    }

    @Override
    protected String getCommand() {
        return "enable";
    }
}
