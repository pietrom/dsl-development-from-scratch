package org.amicofragile.dsls.ffmaster.core;

public class DisableCommand extends FieldCommand {
    public DisableCommand(String fieldId) {
        super(fieldId);
    }

    @Override
    protected String getCommand() {
        return "disable";
    }
}
