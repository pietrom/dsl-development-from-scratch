package org.amicofragile.dsls.ffmaster.core;

public class HideCommand extends FieldCommand {
    public HideCommand(String fieldId) {
        super(fieldId);
    }

    @Override
    protected String getCommand() {
        return "hide";
    }
}
