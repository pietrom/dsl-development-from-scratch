package org.amicofragile.dsls.ffmaster.core;

public class ShowCommand extends FieldCommand {
    public ShowCommand(String fieldId) {
        super(fieldId);
    }

    @Override
    protected String getCommand() {
        return "show";
    }
}
