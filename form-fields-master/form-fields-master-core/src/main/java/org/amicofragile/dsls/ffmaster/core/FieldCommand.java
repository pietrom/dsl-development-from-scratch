package org.amicofragile.dsls.ffmaster.core;

import java.util.Objects;

public abstract class FieldCommand implements WidgetCommand {
    private final String fieldId;

    protected FieldCommand(String fieldId) {
        this.fieldId = fieldId;
    }

    protected abstract String getCommand();

    @Override
    public String toCommand() {
        return String.format("%s('%s')", getCommand(), fieldId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FieldCommand that = (FieldCommand) o;
        return Objects.equals(getCommand(), that.getCommand()) && Objects.equals(fieldId, that.fieldId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCommand(), fieldId);
    }
}
