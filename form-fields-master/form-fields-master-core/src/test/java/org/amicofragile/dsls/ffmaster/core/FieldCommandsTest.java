package org.amicofragile.dsls.ffmaster.core;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class FieldCommandsTest {
    @Test
    public void canEnableFields() {
        String cmd = new EnableCommand("theId").toCommand();
        assertThat(cmd, is(equalTo("enable('theId')")));
    }

    @Test
    public void canDisableFields() {
        String cmd = new DisableCommand("theId").toCommand();
        assertThat(cmd, is(equalTo("disable('theId')")));
    }

    @Test
    public void canShowFields() {
        String cmd = new ShowCommand("theId").toCommand();
        assertThat(cmd, is(equalTo("show('theId')")));
    }

    @Test
    public void canHideFields() {
        String cmd = new HideCommand("theId").toCommand();
        assertThat(cmd, is(equalTo("hide('theId')")));
    }
}
