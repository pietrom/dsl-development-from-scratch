package org.amicofragile.dsls.jhttp;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpUrlConnectionUtil {
    public static Map<String, String> readResponseCookies(HttpURLConnection conn) {
        Map<String, List<String>> headers = conn.getHeaderFields();
        Map<String, String> cookies = new HashMap<>();
        List<String> setCookies = headers.get("set-cookie");
        for (String setCookie : setCookies) {
            String[] fields = setCookie.trim().split("=");
            cookies.put(fields[0], fields[1]);
        }
        return cookies;
    }

    public static String readResponseBody(HttpURLConnection connection) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
        StringBuilder sb = new StringBuilder();
        String s = null;
        while ((s = reader.readLine()) != null) {
            sb.append(s);
        }
        return sb.toString();
    }

    public static HttpURLConnection get(String url) throws Exception {
        return openConnection(url, "GET");
    }

    public static HttpURLConnection post(String url) throws Exception {
        return openConnection(url, "POST");
    }

    public static HttpURLConnection postJson(String url, String body) throws Exception {
        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-type", "application/json");
        conn.setDoOutput(true);
        conn.connect();
        try(OutputStream os = conn.getOutputStream()) {
            byte[] input = body.getBytes("utf-8");
            os.write(input, 0, input.length);
        }
        return conn;
    }

    public static HttpURLConnection openConnection(String url, String method) throws Exception {
        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
        conn.setRequestMethod(method);
        conn.connect();
        return conn;
    }
}
