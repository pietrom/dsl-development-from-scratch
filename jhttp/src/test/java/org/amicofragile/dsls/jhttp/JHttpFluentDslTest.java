package org.amicofragile.dsls.jhttp;

import org.amicofragile.dsls.commons.http.PortUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.amicofragile.dsls.jhttp.JHttpFluentDsl.jhttp;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import org.mockserver.integration.ClientAndServer;

import static org.junit.Assert.assertThat;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public class JHttpFluentDslTest {
    private static int localPort;
    private static ClientAndServer clientAndServer;

    @BeforeClass
    public static void InitMockServer() {
        localPort = PortUtils.nextFreePort(10000, 11000);
        clientAndServer = startClientAndServer(localPort);

        clientAndServer.when(
                request()
                        .withMethod("GET")
                        .withPath("/test")
                        .withHeader("Authorization", "Bearer 12345654321")
                        .withCookie("UserId", "pietrom")
        ).respond(
                response()
                        .withStatusCode(200)
                        .withBody("Test passed!")
                        .withCookie(
                                "sessionId", "1234321"
                        )
                        .withCookie("foo", "bar")
                        .withHeader(
                                "Location", "https://www.mock-server.com"
                        )
        );
    }

    @AfterClass
    public static void TeardownMockServer() {
        clientAndServer.stop();
    }

    @Test
    public void getStatusCodeWithNaifDsl() throws Exception {
        JHttpResponse response = jhttp(String.format("http://localhost:%d/test", localPort))
                .withHeader("Authorization", "Bearer 12345654321")
                .withCookie("UserId", "pietrom")
                .get();
        assertThat(response.getStatusCode(), is(equalTo(200)));
    }

    @Test
    public void getResponseBodyWithNaifDsl() throws Exception {
        JHttpResponse response = jhttp(String.format("http://localhost:%d/test", localPort))
                .withHeader("Authorization", "Bearer 12345654321")
                .withCookie("UserId", "pietrom")
                .get();
        assertThat(response.getResponseBody(), is(equalTo("Test passed!")));
    }

    @Test
    public void getResponseHeaderWithNaifDsl() throws Exception {
        JHttpResponse response = jhttp(String.format("http://localhost:%d/test", localPort))
                .withHeader("Authorization", "Bearer 12345654321")
                .withCookie("UserId", "pietrom")
                .get();
        assertThat(response.getHeader("Location"), is(equalTo("https://www.mock-server.com")));
    }

    @Test
    public void getCookiesWithNaifDsl() throws Exception {
        JHttpResponse response = jhttp(String.format("http://localhost:%d/test", localPort))
                .withHeader("Authorization", "Bearer 12345654321")
                .withCookie("UserId", "pietrom")
                .get();
        assertThat(response.getCookie("sessionId"), is(equalTo("1234321")));
        assertThat(response.getCookie("foo"), is(equalTo("bar")));
    }
}
