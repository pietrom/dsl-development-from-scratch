package org.amicofragile.dsls.jhttp;

import org.amicofragile.dsls.commons.http.PortUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockserver.integration.ClientAndServer;

import java.net.HttpURLConnection;
import java.util.Map;

import static org.amicofragile.dsls.jhttp.HttpUrlConnectionUtil.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public class GettingStartedTest {
    private static int localPort;
    private static ClientAndServer clientAndServer;

    @BeforeClass
    public static void InitMockServer() {
        localPort = PortUtils.nextFreePort(10000, 11000);
        clientAndServer = startClientAndServer(localPort);

        clientAndServer.when(
                request()
                        .withMethod("GET")
                        .withPath("/test")
        ).respond(
                response()
                        .withStatusCode(200)
                        .withBody("Test passed!")
                        .withCookie(
                                "sessionId", "1234321"
                        )
                        .withCookie("foo", "bar")
                        .withHeader(
                                "Location", "https://www.mock-server.com"
                        )
        );
    }

    @AfterClass
    public static void TeardownMockServer() {
        clientAndServer.stop();
    }

    @Test
    public void canReadHttpResponseStatus() throws Exception {
        HttpURLConnection conn = get(String.format("http://localhost:%d/test", localPort));
        assertThat(conn.getResponseCode(), is(equalTo(200)));
    }

    @Test
    public void canReadHttpResponseBody() throws Exception {
        HttpURLConnection conn = get(String.format("http://localhost:%d/test", localPort));
        String responseBody = readResponseBody(conn);
        assertThat(responseBody, is(equalTo("Test passed!")));
    }

    @Test
    public void canReadHttpResponseHeader() throws Exception {
        HttpURLConnection conn = get(String.format("http://localhost:%d/test", localPort));
        assertThat(conn.getHeaderField("Location"), is(equalTo("https://www.mock-server.com")));
    }

    @Test
    public void canReadHttpResponseCookies() throws Exception {
        HttpURLConnection conn = get(String.format("http://localhost:%d/test", localPort));
        Map<String, String> cookies = readResponseCookies(conn);
        assertThat(cookies.get("sessionId"), is(equalTo("1234321")));
        assertThat(cookies.get("foo"), is(equalTo("bar")));
    }
}
