package org.amicofragile.dsls.jhttp;

import org.amicofragile.dsls.commons.http.PortUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockserver.integration.ClientAndServer;

import java.net.HttpURLConnection;
import java.util.Map;

import static org.amicofragile.dsls.jhttp.HttpUrlConnectionUtil.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.mockserver.model.StringBody.exact;

public class PostingStartedTest {
    private static int localPort;
    private static ClientAndServer clientAndServer;
    private static final String POST_BODY = "{ \"text\": \"A bit of text here\" }";
    private static final String POST_RESPONSE_BODY = "{ \"status\": \"Test passed!\" }";
    private static final String CONTENT_TYPE = "Content-Type";

    @BeforeClass
    public static void InitMockServer() throws Exception {
        localPort = PortUtils.nextFreePort(10000, 11000);
        clientAndServer = startClientAndServer(localPort);

        clientAndServer.when(
                request()
                        .withMethod("POST")
                        .withPath("/test")
                        .withHeader(CONTENT_TYPE, "application/json")
                        .withBody(exact(POST_BODY))
                )
                .respond(
                        response()
                                .withCookie("sessionId", "1234321")
                                .withCookie("foo", "bar")
                                .withHeader(CONTENT_TYPE, "application/json")
                                .withHeader("Location", "https://www.mock-server.com")
                                .withStatusCode(200)
                                .withBody(POST_RESPONSE_BODY)
                );
    }

    @AfterClass
    public static void TeardownMockServer() {
        clientAndServer.stop();
    }

    @Test
    public void canReadHttpResponseStatus() throws Exception {
        HttpURLConnection conn = postJson(String.format("http://localhost:%d/test", localPort), POST_BODY);
        assertThat(conn.getResponseCode(), is(equalTo(200)));
    }

    @Test
    public void canReadHttpResponseBody() throws Exception {
        HttpURLConnection conn = postJson(String.format("http://localhost:%d/test", localPort), POST_BODY);
        String responseBody = readResponseBody(conn);
        assertThat(responseBody, is(equalTo(POST_RESPONSE_BODY)));
    }

    @Test
    public void canReadHttpResponseHeader() throws Exception {
        HttpURLConnection conn = postJson(String.format("http://localhost:%d/test", localPort), POST_BODY);
        assertThat(conn.getHeaderField("Location"), is(equalTo("https://www.mock-server.com")));
    }

    @Test
    public void canReadHttpResponseContentType() throws Exception {
        HttpURLConnection conn = postJson(String.format("http://localhost:%d/test", localPort), POST_BODY);
        assertThat(conn.getHeaderField("Content-Type"), is(equalTo("application/json")));
    }

    @Test
    public void canReadHttpResponseCookies() throws Exception {
        HttpURLConnection conn = postJson(String.format("http://localhost:%d/test", localPort), POST_BODY);
        Map<String, String> cookies = readResponseCookies(conn);
        assertThat(cookies.get("sessionId"), is(equalTo("1234321")));
        assertThat(cookies.get("foo"), is(equalTo("bar")));
    }
}
