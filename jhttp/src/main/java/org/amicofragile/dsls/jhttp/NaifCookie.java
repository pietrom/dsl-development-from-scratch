package org.amicofragile.dsls.jhttp;

public class NaifCookie {
    private final String key;
    private final String value;

    NaifCookie(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public static NaifCookie cookie(String key, String value) {
        return new NaifCookie(key, value);
    }
}
