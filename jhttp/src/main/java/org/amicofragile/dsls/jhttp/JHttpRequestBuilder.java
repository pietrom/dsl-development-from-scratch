package org.amicofragile.dsls.jhttp;

import org.amicofragile.dsls.commons.http.HttpUrlConnectionFacade;

import java.util.LinkedHashMap;
import java.util.Map;

public class JHttpRequestBuilder {
    private final String url;
    private final Map<String, String> cookies = new LinkedHashMap<>();
    private final Map<String, String> headers = new LinkedHashMap<>();

    public JHttpRequestBuilder(String url) {
        this.url = url;
    }

    public JHttpRequestBuilder withHeader(String key, String value) {
        headers.put(key, value);
        return this;
    }

    public JHttpRequestBuilder withCookie(String key, String value) {
        cookies.put(key, value);
        return this;
    }

    public JHttpResponse get() throws Exception {
        return new JHttpResponse(HttpUrlConnectionFacade.openGet(url, headers, cookies));
    }
}
