package org.amicofragile.dsls.jhttp;

public class JHttpFluentDsl {
    public static JHttpRequestBuilder jhttp(String url) {
        return new JHttpRequestBuilder(url);
    }
}
