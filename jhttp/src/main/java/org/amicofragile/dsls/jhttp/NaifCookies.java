package org.amicofragile.dsls.jhttp;

import java.util.Arrays;
import java.util.Collection;

public class NaifCookies {
    private final Collection<NaifCookie> cookies;

    public NaifCookies(Collection<NaifCookie> cookies) {
        this.cookies = cookies;
    }

    public Collection<NaifCookie> getCookies() {
        return cookies;
    }

    public static NaifCookies cookies(NaifCookie... cookies) {
        return new NaifCookies(Arrays.asList(cookies));
    }
}
