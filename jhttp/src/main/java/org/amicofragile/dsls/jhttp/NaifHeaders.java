package org.amicofragile.dsls.jhttp;

import java.util.Arrays;
import java.util.Collection;

public class NaifHeaders {
    private final Collection<NaifHeader> headers;

    public NaifHeaders(Collection<NaifHeader> headers) {
        this.headers = headers;
    }

    public Collection<NaifHeader> getHeaders() {
        return headers;
    }

    public static NaifHeaders headers(NaifHeader... headers) {
        return new NaifHeaders(Arrays.asList(headers));
    }
}
