package org.amicofragile.dsls.jhttp;

public class NaifHeader {
    private final String key;
    private final String value;

    public NaifHeader(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public static NaifHeader header(String key, String value) {
        return new NaifHeader(key, value);
    }
}
