package org.amicofragile.dsls.jhttp;

import java.util.stream.Collectors;

public class JHttpNaifDsl {
    public static JHttpResponse get(String url, NaifHeaders headers, NaifCookies cookies) throws Exception {
        return new JHttpResponse(org.amicofragile.dsls.commons.http.HttpUrlConnectionFacade.openGet(url,
                headers.getHeaders().stream().collect(Collectors.toMap(NaifHeader::getKey, NaifHeader::getValue)),
                cookies.getCookies().stream().collect(Collectors.toMap(NaifCookie::getKey, NaifCookie::getValue))));
    }
}